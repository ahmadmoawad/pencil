## Pencil backend

The assignment written in Node.js and Mongo DB.

### Technologies

The technologies and the libraries which used in the project

01. Node.js
02. Express.js
03. Morgan
04. Body-Parser
05. Colos
06. Express
07. Lodash
08. Method-override
09. Mongoose
10. Mongo DB
11. Chai
12. Mocha
13. SuperTest

### Endpoints APIs

* Search

|Description|Link|
|---|---|
|List all question under topic/annotation|http://35.183.179.20:3000//search?q={annotation}|

. You list the type of machines by replacing {annotation} with the annotation that you want to search `Mitochondria`

### Live demo

I have hosted the solution on `AWS` for live demo in the link

* Search

|Description|Link|
|---|---|
|List all question under Mitochondria|http://35.183.179.20:3000/search?q=Mitochondria|
|List all question under Carbohydrate metabolism|http://35.183.179.20:3000/search?q=Carbohydrate%20metabolism|

---

### Solutions for tree representation

01. Use String path with comma separated to point to the parent, i.e ,47,10,65,61
    - the schema will be like

    |_id|annotation|parent|
    |---|----------|------|
    |1|annotation_1|Null|
    |2|annotation_2|,1,|
    |3|annotation_3|,1,2|
    |4|annotation_4|,3,|
    |5|annotation_5|,3,4|

02. Use parent id and traverse until reach parent of id 0, you can apply `while-loop` for search until reach the root node

    |_id|annotation|parent|
    |---|----------|------|
    |1|annotation_1|0|
    |2|annotation_2|1|
    |3|annotation_3|2|
    |4|annotation_4|3|
    |5|annotation_5|4|

---

### Database implementation

* Many-to-Many relationship between `Questions` and `Annotations` by this approach you are save space rather than insert the duplication data into one table in the Database.

---

### How you can install the project?

You can download the source `source.zip` from the Github and unzip the source folder then type the following commands:

01. Install dependencies

``` shell
npm install
```

02. Change the environment to `development`

``` shell
export NODE_ENV=development
```

03. Run the seed for seeding the development database

``` shell
node seed.js
```

04. Run the mongodb demon on the port `27017`

05. Run the application

``` shell
npm start
```

### How you can run the test?

01. Change the environment to `testing`

``` shell
export NODE_ENV=testing
```

02. Run the seed for seeding the test database

``` shell
node seed.js
```

03. Run the testing

``` shell
npm test
```

## Example

``` node
const mongoose = require("mongoose");
const config = require("./server/config");
const logger = require("./server/util/logger");
const topic_data = require("./data/topics");
const question_data = require("./data/questions");
const QuestionModel = require("./server/api/questions/model");

async function insert_topic() {
  logger.log("insert_topic...");
  for (let topic of topic_data) {
    await topic.save({});
  }
  logger.log("done insert_topic...");
}

async function insert_questions() {
  logger.log("insert_questions...");
  let index = 0;
  for (let question of question_data) {
    await question.save((err, r) => {
      if (index === question_data.length - 1) {
        logger.log("Inserting questions are done");
        mongoose.disconnect();
      }
      index++;
    });
  }
  logger.log("done insert_questions...");
}

(async () => {
  (async () => {
    var client = null;
    await mongoose.connect(
      config.db.url,
      { useNewUrlParser: true },
      (err, res) => {
        client = res;
      }
    );

    logger.log("connected to db...");
    await mongoose.connection.dropDatabase();
    await insert_topic();
    await insert_questions();
  })();
})();

```

### Working directory

You should have the following directory before run `npm install`

``` tree
.
├── README.md
├── data
│   ├── questions.js
│   └── topics.js
├── index.js
├── output
├── package-lock.json
├── package.json
├── seed.js
├── server
│   ├── api
│   │   ├── index.js
│   │   ├── questions
│   │   │   ├── controller.js
│   │   │   ├── model.js
│   │   │   └── router.js
│   │   └── topics
│   │       └── model.js
│   ├── config
│   │   ├── development.js
│   │   ├── index.js
│   │   ├── production.js
│   │   └── testing.js
│   ├── index.js
│   ├── middleware
│   │   ├── err.js
│   │   └── index.js
│   └── util
│       ├── createRouter.js
│       └── logger.js
└── test
    └── question_test.js

9 directories, 23 files

```

## Important notes:

01. All annotations have been inserted under topics table
02. Questions until id 30 have been inserted due to tight of time, not all of them have been inserted

## Future work

*Note: Due to the tight of the time, I have skipped these two points*

01. Dockerized the solution and build image for the backend
02. Make `docker-compose` file to link the backend image with MonogDB image
