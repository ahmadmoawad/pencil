const mongoose = require("mongoose");
const config = require("./server/config");
const logger = require("./server/util/logger");
const topic_data = require("./data/topics");
const question_data = require("./data/questions");
const QuestionModel = require("./server/api/questions/model");

async function insert_topic() {
  logger.log("insert_topic...");
  for (let topic of topic_data) {
    await topic.save({});
  }
  logger.log("done insert_topic...");
}

async function insert_questions() {
  logger.log("insert_questions...");
  let index = 0;
  for (let question of question_data) {
    await question.save((err, r) => {
      if (index === question_data.length - 1) {
        logger.log("Inserting questions are done");
        // QuestionModel.find({})
        //   .populate("annotations")
        //   .exec((err, result) => {
        //     console.log("result: " + result);
        //   });
        mongoose.disconnect();
      }
      index++;
    });
  }
  logger.log("done insert_questions...");
}

(async () => {
  (async () => {
    var client = null;
    await mongoose.connect(
      config.db.url,
      { useNewUrlParser: true },
      (err, res) => {
        client = res;
      }
    );

    logger.log("connected to db...");
    await mongoose.connection.dropDatabase();
    await insert_topic();
    await insert_questions();
  })();
})();
