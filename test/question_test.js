const app = require("../server");
const request = require("supertest");
const expect = require("chai").expect;

describe("[questions]", () => {
  it("should Mitochondria questions", (done) => {
    request(app)
      .get("/search?q=Mitochondria")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .end((err, res) => {
        expect(res.body).to.be.an("array");
        expect(res.body.length).to.eql(2);
        expect(res.body[0]).to.eql(11);
        expect(res.body[1]).to.eql(59);
        done();
      });
  });
  it("should Carbohydrate metabolism questions", (done) => {
    request(app)
      .get("/search?q=Carbohydrate metabolism")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .end((err, res) => {
        expect(res.body).to.be.an("array");
        expect(res.body.length).to.eql(2);
        expect(res.body[0]).to.eql(131);
        expect(res.body[1]).to.eql(184);
        done();
      });
  });
  it("should not found questions", (done) => {
    request(app)
      .get("/searched")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(404)
      .end((err, res) => {
        expect(res.status).to.eql(404);
        done();
      });
  });
});
