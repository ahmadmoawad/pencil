const QuestionModel = require("../server/api/questions/model");
const topic_data = require("./topics");

const question_data = [
  new QuestionModel({
    question_id: 1,
    annotations: [topic_data[22], topic_data[24], topic_data[72]],
  }),
  new QuestionModel({
    question_id: 2,
    annotations: [
      topic_data[148],
      topic_data[157],
      topic_data[134],
      topic_data[84],
    ],
  }),
  new QuestionModel({
    question_id: 3,
    annotations: [
      topic_data[73],
      topic_data[19],
      topic_data[105],
      topic_data[98],
      topic_data[111],
    ],
  }),
  new QuestionModel({
    question_id: 4,
    annotations: [topic_data[66], topic_data[116], topic_data[56]],
  }),
  new QuestionModel({
    question_id: 5,
    annotations: [topic_data[25], topic_data[30]],
  }),
  new QuestionModel({
    question_id: 6,
    annotations: [topic_data[36], topic_data[33]],
  }),
  new QuestionModel({
    question_id: 7,
    annotations: [
      topic_data[92],
      topic_data[100],
      topic_data[141],
      topic_data[151],
    ],
  }),
  new QuestionModel({
    question_id: 8,
    annotations: [topic_data[143]],
  }),
  new QuestionModel({
    question_id: 9,
    annotations: [topic_data[162], topic_data[168]],
  }),
  new QuestionModel({
    question_id: 10,
    annotations: [topic_data[29]],
  }),
  new QuestionModel({
    question_id: 11,
    annotations: [topic_data[147]],
  }),
  new QuestionModel({
    question_id: 12,
    annotations: [topic_data[100], topic_data[48]],
  }),
  new QuestionModel({
    question_id: 13,
    annotations: [topic_data[22]],
  }),
  new QuestionModel({
    question_id: 14,
    annotations: [topic_data[121], topic_data[155]],
  }),
  new QuestionModel({
    question_id: 15,
    annotations: [
      topic_data[102],
      topic_data[86],
      topic_data[77],
      topic_data[63],
      topic_data[140],
    ],
  }),
  new QuestionModel({
    question_id: 16,
    annotations: [topic_data[178]],
  }),
  new QuestionModel({
    question_id: 17,
    annotations: [topic_data[75]],
  }),
  new QuestionModel({
    question_id: 18,
    annotations: [topic_data[133]],
  }),
  new QuestionModel({
    question_id: 19,
    annotations: [topic_data[76], topic_data[145], topic_data[29]],
  }),
  new QuestionModel({
    question_id: 20,
    annotations: [
      topic_data[158],
      topic_data[156],
      topic_data[87],
      topic_data[115],
    ],
  }),
  new QuestionModel({
    question_id: 21,
    annotations: [topic_data[143]],
  }),
  new QuestionModel({
    question_id: 22,
    annotations: [topic_data[65]],
  }),
  new QuestionModel({
    question_id: 23,
    annotations: [
      topic_data[132],
      topic_data[77],
      topic_data[145],
      topic_data[32],
      topic_data[66],
    ],
  }),
  new QuestionModel({
    question_id: 24,
    annotations: [topic_data[86], topic_data[47], topic_data[158]],
  }),
  new QuestionModel({
    question_id: 25,
    annotations: [topic_data[98], topic_data[98], topic_data[165]],
  }),
  new QuestionModel({
    question_id: 26,
    annotations: [topic_data[177]],
  }),
  new QuestionModel({
    question_id: 27,
    annotations: [topic_data[70], topic_data[57], topic_data[122]],
  }),
  new QuestionModel({
    question_id: 28,
    annotations: [topic_data[61]],
  }),
  new QuestionModel({
    question_id: 29,
    annotations: [topic_data[89]],
  }),
  new QuestionModel({
    question_id: 30,
    annotations: [topic_data[76]],
  }),
  new QuestionModel({
    question_id: 31,
    annotations: [
      topic_data[110],
      topic_data[121],
      topic_data[173],
      topic_data[66110],
    ],
  }),
  new QuestionModel({
    question_id: 32,
    annotations: [topic_data[120]],
  }),
  new QuestionModel({
    question_id: 33,
    annotations: [topic_data[130]],
  }),
  new QuestionModel({
    question_id: 34,
    annotations: [topic_data[114], topic_data[165]],
  }),
  new QuestionModel({
    question_id: 35,
    annotations: [topic_data[77]],
  }),
  new QuestionModel({
    question_id: 36,
    annotations: [topic_data[125]],
  }),
  new QuestionModel({
    question_id: 37,
    annotations: [
      topic_data[75],
      topic_data[94],
      topic_data[45],
      topic_data[126],
      topic_data[153],
    ],
  }),
  new QuestionModel({
    question_id: 38,
    annotations: [topic_data[30]],
  }),
  new QuestionModel({
    question_id: 39,
    annotations: [topic_data[63]],
  }),
  new QuestionModel({
    question_id: 40,
    annotations: [topic_data[22]],
  }),
  new QuestionModel({
    question_id: 41,
    annotations: [topic_data[146], topic_data[88]],
  }),
  new QuestionModel({
    question_id: 42,
    annotations: [topic_data[117]],
  }),
  new QuestionModel({
    question_id: 43,
    annotations: [topic_data[115]],
  }),
  new QuestionModel({
    question_id: 44,
    annotations: [topic_data[114]],
  }),
  new QuestionModel({
    question_id: 45,
    annotations: [
      topic_data[146],
      topic_data[160],
      topic_data[100],
      topic_data[160],
    ],
  }),
  new QuestionModel({
    question_id: 46,
    annotations: [topic_data[167]],
  }),
  new QuestionModel({
    question_id: 47,
    annotations: [topic_data[35]],
  }),
  new QuestionModel({
    question_id: 48,
    annotations: [topic_data[140], topic_data[131], topic_data[89]],
  }),
  new QuestionModel({
    question_id: 49,
    annotations: [topic_data[96], topic_data[120]],
  }),
  new QuestionModel({
    question_id: 50,
    annotations: [topic_data[145]],
  }),
  new QuestionModel({
    question_id: 51,
    annotations: [topic_data[69], topic_data[119]],
  }),
  new QuestionModel({
    question_id: 52,
    annotations: [topic_data[129], topic_data[94]],
  }),
  new QuestionModel({
    question_id: 53,
    annotations: [topic_data[76], topic_data[92]],
  }),
  new QuestionModel({
    question_id: 54,
    annotations: [topic_data[92]],
  }),
  new QuestionModel({
    question_id: 55,
    annotations: [topic_data[73], topic_data[29]],
  }),
  new QuestionModel({
    question_id: 56,
    annotations: [
      topic_data[168],
      topic_data[101],
      topic_data[157],
      topic_data[152],
    ],
  }),
  new QuestionModel({
    question_id: 57,
    annotations: [topic_data[134]],
  }),
  new QuestionModel({
    question_id: 58,
    annotations: [topic_data[89], topic_data[116], topic_data[68]],
  }),
  new QuestionModel({
    question_id: 59,
    annotations: [topic_data[114], topic_data[34], topic_data[147]],
  }),
  new QuestionModel({
    question_id: 60,
    annotations: [topic_data[32]],
  }),
  new QuestionModel({
    question_id: 61,
    annotations: [
      topic_data[154],
      topic_data[176],
      topic_data[66],
      topic_data[22],
    ],
  }),
  new QuestionModel({
    question_id: 62,
    annotations: [topic_data[35]],
  }),
  new QuestionModel({
    question_id: 63,
    annotations: [topic_data[167]],
  }),
  new QuestionModel({
    question_id: 64,
    annotations: [topic_data[70], topic_data[145]],
  }),
  new QuestionModel({
    question_id: 65,
    annotations: [topic_data[97], topic_data[91]],
  }),
  new QuestionModel({
    question_id: 66,
    annotations: [
      topic_data[136],
      topic_data[136],
      topic_data[25],
      topic_data[141],
      topic_data[40],
    ],
  }),
  new QuestionModel({
    question_id: 67,
    annotations: [topic_data[30]],
  }),
  new QuestionModel({
    question_id: 68,
    annotations: [topic_data[55]],
  }),
  new QuestionModel({
    question_id: 69,
    annotations: [topic_data[117], topic_data[93]],
  }),
  new QuestionModel({
    question_id: 70,
    annotations: [topic_data[133], topic_data[107]],
  }),
  new QuestionModel({
    question_id: 71,
    annotations: [topic_data[60]],
  }),
  new QuestionModel({
    question_id: 72,
    annotations: [topic_data[115]],
  }),
  new QuestionModel({
    question_id: 73,
    annotations: [topic_data[92], topic_data[114], topic_data[66]],
  }),
  new QuestionModel({
    question_id: 74,
    annotations: [topic_data[58]],
  }),
  new QuestionModel({
    question_id: 75,
    annotations: [topic_data[136], topic_data[82]],
  }),
  new QuestionModel({
    question_id: 76,
    annotations: [topic_data[143], topic_data[162]],
  }),
  new QuestionModel({
    question_id: 77,
    annotations: [topic_data[29], topic_data[126]],
  }),
  new QuestionModel({
    question_id: 78,
    annotations: [topic_data[170], topic_data[158]],
  }),
  new QuestionModel({
    question_id: 79,
    annotations: [topic_data[110], topic_data[135]],
  }),
  new QuestionModel({
    question_id: 80,
    annotations: [topic_data[36], topic_data[117]],
  }),
  new QuestionModel({
    question_id: 81,
    annotations: [topic_data[126]],
  }),
  new QuestionModel({
    question_id: 82,
    annotations: [topic_data[38]],
  }),
  new QuestionModel({
    question_id: 82,
    annotations: [topic_data[19]],
  }),
  new QuestionModel({
    question_id: 83,
    annotations: [topic_data[71], topic_data[132], topic_data[143]],
  }),
  new QuestionModel({
    question_id: 84,
    annotations: [topic_data[169]],
  }),
  new QuestionModel({
    question_id: 85,
    annotations: [topic_data[124]],
  }),
  new QuestionModel({
    question_id: 86,
    annotations: [topic_data[103], topic_data[135]],
  }),
  new QuestionModel({
    question_id: 87,
    annotations: [
      topic_data[109],
      topic_data[172],
      topic_data[74],
      topic_data[140],
      topic_data[131],
    ],
  }),
  new QuestionModel({
    question_id: 88,
    annotations: [topic_data[60]],
  }),
  new QuestionModel({
    question_id: 89,
    annotations: [
      topic_data[36],
      topic_data[57],
      topic_data[109],
      topic_data[107],
    ],
  }),
  new QuestionModel({
    question_id: 90,
    annotations: [topic_data[32]],
  }),
  new QuestionModel({
    question_id: 91,
    annotations: [topic_data[91]],
  }),
  new QuestionModel({
    question_id: 92,
    annotations: [
      topic_data[103],
      topic_data[81],
      topic_data[155],
      topic_data[83],
    ],
  }),
  new QuestionModel({
    question_id: 93,
    annotations: [topic_data[135]],
  }),
  new QuestionModel({
    question_id: 94,
    annotations: [topic_data[18]],
  }),
  new QuestionModel({
    question_id: 95,
    annotations: [],
  }),
  new QuestionModel({
    question_id: 96,
    annotations: [topic_data[149]],
  }),
  new QuestionModel({
    question_id: 96,
    annotations: [topic_data[149]],
  }),
  new QuestionModel({
    question_id: 97,
    annotations: [topic_data[78], topic_data[175]],
  }),
  new QuestionModel({
    question_id: 98,
    annotations: [topic_data[131]],
  }),
  new QuestionModel({
    question_id: 99,
    annotations: [topic_data[116]],
  }),
  new QuestionModel({
    question_id: 100,
    annotations: [topic_data[146]],
  }),
  new QuestionModel({
    question_id: 101,
    annotations: [topic_data[104], topic_data[67]],
  }),
  new QuestionModel({
    question_id: 102,
    annotations: [topic_data[178]],
  }),
  new QuestionModel({
    question_id: 103,
    annotations: [topic_data[117], topic_data[129]],
  }),
  new QuestionModel({
    question_id: 104,
    annotations: [topic_data[58]],
  }),
  new QuestionModel({
    question_id: 105,
    annotations: [topic_data[114], topic_data[150]],
  }),
  new QuestionModel({
    question_id: 106,
    annotations: [
      topic_data[126],
      topic_data[48],
      topic_data[84],
      topic_data[88],
      topic_data[97],
    ],
  }),
  new QuestionModel({
    question_id: 107,
    annotations: [
      topic_data[63],
      topic_data[43],
      topic_data[86],
      topic_data[110],
    ],
  }),
  new QuestionModel({
    question_id: 108,
    annotations: [topic_data[60]],
  }),
  new QuestionModel({
    question_id: 109,
    annotations: [
      topic_data[40],
      topic_data[90],
      topic_data[94],
      topic_data[124],
      topic_data[45],
    ],
  }),
  new QuestionModel({
    question_id: 110,
    annotations: [topic_data[62], topic_data[36]],
  }),
  new QuestionModel({
    question_id: 111,
    annotations: [
      topic_data[48],
      topic_data[153],
      topic_data[67],
      topic_data[55],
      topic_data[177],
    ],
  }),
  new QuestionModel({
    question_id: 112,
    annotations: [topic_data[160]],
  }),
  new QuestionModel({
    question_id: 113,
    annotations: [topic_data[136]],
  }),
  new QuestionModel({
    question_id: 114,
    annotations: [topic_data[69]],
  }),
  new QuestionModel({
    question_id: 115,
    annotations: [topic_data[145]],
  }),
  new QuestionModel({
    question_id: 116,
    annotations: [topic_data[153], topic_data[129]],
  }),
  new QuestionModel({
    question_id: 117,
    annotations: [
      topic_data[124],
      topic_data[56],
      topic_data[151],
      topic_data[175],
    ],
  }),
  new QuestionModel({
    question_id: 118,
    annotations: [
      topic_data[149],
      topic_data[114],
      topic_data[72],
      topic_data[177],
    ],
  }),
  new QuestionModel({
    question_id: 119,
    annotations: [topic_data[44], topic_data[70]],
  }),
  new QuestionModel({
    question_id: 120,
    annotations: [
      topic_data[105],
      topic_data[158],
      topic_data[30],
      topic_data[154],
      topic_data[69],
    ],
  }),
  new QuestionModel({
    question_id: 121,
    annotations: [
      topic_data[72],
      topic_data[103],
      topic_data[149],
      topic_data[133],
    ],
  }),
  new QuestionModel({
    question_id: 122,
    annotations: [
      topic_data[61],
      topic_data[70],
      topic_data[82],
      topic_data[96],
    ],
  }),
  new QuestionModel({
    question_id: 123,
    annotations: [
      topic_data[148],
      topic_data[164],
      topic_data[67],
      topic_data[127],
      topic_data[62],
    ],
  }),
  new QuestionModel({
    question_id: 124,
    annotations: [topic_data[117]],
  }),
  new QuestionModel({
    question_id: 125,
    annotations: [
      topic_data[104],
      topic_data[77],
      topic_data[38],
      topic_data[99],
    ],
  }),
  new QuestionModel({
    question_id: 126,
    annotations: [topic_data[58], topic_data[133]],
  }),
  new QuestionModel({
    question_id: 127,
    annotations: [topic_data[172], topic_data[175], topic_data[29]],
  }),
  new QuestionModel({
    question_id: 128,
    annotations: [topic_data[155]],
  }),
  new QuestionModel({
    question_id: 129,
    annotations: [topic_data[73], topic_data[173], topic_data[169]],
  }),
  new QuestionModel({
    question_id: 130,
    annotations: [topic_data[102], topic_data[83]],
  }),
  new QuestionModel({
    question_id: 131,
    annotations: [topic_data[58], topic_data[163]],
  }),
  new QuestionModel({
    question_id: 132,
    annotations: [topic_data[150]],
  }),
  new QuestionModel({
    question_id: 133,
    annotations: [topic_data[99], topic_data[165]],
  }),
  new QuestionModel({
    question_id: 134,
    annotations: [topic_data[153]],
  }),
  new QuestionModel({
    question_id: 135,
    annotations: [topic_data[63]],
  }),
  new QuestionModel({
    question_id: 136,
    annotations: [topic_data[33]],
  }),
  new QuestionModel({
    question_id: 137,
    annotations: [topic_data[122]],
  }),
  new QuestionModel({
    question_id: 138,
    annotations: [topic_data[100]],
  }),
  new QuestionModel({
    question_id: 139,
    annotations: [topic_data[150]],
  }),
  new QuestionModel({
    question_id: 140,
    annotations: [topic_data[68]],
  }),
  new QuestionModel({
    question_id: 141,
    annotations: [topic_data[127]],
  }),
  new QuestionModel({
    question_id: 142,
    annotations: [topic_data[143]],
  }),
  new QuestionModel({
    question_id: 143,
    annotations: [topic_data[129]],
  }),
  new QuestionModel({
    question_id: 144,
    annotations: [topic_data[134], topic_data[175], topic_data[133]],
  }),
  new QuestionModel({
    question_id: 145,
    annotations: [topic_data[63], topic_data[178]],
  }),
  new QuestionModel({
    question_id: 146,
    annotations: [topic_data[25]],
  }),
  new QuestionModel({
    question_id: 147,
    annotations: [topic_data[68], topic_data[152]],
  }),
  new QuestionModel({
    question_id: 148,
    annotations: [topic_data[111]],
  }),
  new QuestionModel({
    question_id: 149,
    annotations: [topic_data[106]],
  }),
  new QuestionModel({
    question_id: 150,
    annotations: [topic_data[110]],
  }),
  new QuestionModel({
    question_id: 151,
    annotations: [topic_data[168], topic_data[81]],
  }),
  new QuestionModel({
    question_id: 152,
    annotations: [topic_data[38], topic_data[86]],
  }),
  new QuestionModel({
    question_id: 153,
    annotations: [topic_data[71], topic_data[101], topic_data[31]],
  }),
  new QuestionModel({
    question_id: 154,
    annotations: [topic_data[169]],
  }),
  new QuestionModel({
    question_id: 155,
    annotations: [
      topic_data[90],
      topic_data[117],
      topic_data[175],
      topic_data[109],
      topic_data[164],
    ],
  }),
  new QuestionModel({
    question_id: 156,
    annotations: [topic_data[19], topic_data[148]],
  }),
  new QuestionModel({
    question_id: 157,
    annotations: [topic_data[132]],
  }),
  new QuestionModel({
    question_id: 158,
    annotations: [
      topic_data[93],
      topic_data[177],
      topic_data[173],
      topic_data[178],
      topic_data[165],
    ],
  }),
  new QuestionModel({
    question_id: 159,
    annotations: [topic_data[122]],
  }),
  new QuestionModel({
    question_id: 160,
    annotations: [topic_data[105]],
  }),
  new QuestionModel({
    question_id: 161,
    annotations: [topic_data[107], topic_data[67]],
  }),
  new QuestionModel({
    question_id: 162,
    annotations: [
      topic_data[110],
      topic_data[115],
      topic_data[172],
      topic_data[117],
    ],
  }),
  new QuestionModel({
    question_id: 163,
    annotations: [topic_data[145]],
  }),
  new QuestionModel({
    question_id: 164,
    annotations: [topic_data[141], topic_data[60]],
  }),
  new QuestionModel({
    question_id: 165,
    annotations: [topic_data[175]],
  }),
  new QuestionModel({
    question_id: 166,
    annotations: [topic_data[88]],
  }),
  new QuestionModel({
    question_id: 167,
    annotations: [topic_data[102], topic_data[83]],
  }),
  new QuestionModel({
    question_id: 168,
    annotations: [topic_data[173], topic_data[76]],
  }),
  new QuestionModel({
    question_id: 169,
    annotations: [topic_data[97]],
  }),
  new QuestionModel({
    question_id: 170,
    annotations: [topic_data[88]],
  }),
  new QuestionModel({
    question_id: 171,
    annotations: [
      topic_data[151],
      topic_data[97],
      topic_data[62],
      topic_data[158],
      topic_data[76],
    ],
  }),
  new QuestionModel({
    question_id: 172,
    annotations: [],
  }),
  new QuestionModel({
    question_id: 173,
    annotations: [
      topic_data[97],
      topic_data[60],
      topic_data[68],
      topic_data[62],
    ],
  }),
  new QuestionModel({
    question_id: 173,
    annotations: [
      topic_data[97],
      topic_data[60],
      topic_data[68],
      topic_data[62],
    ],
  }),
  new QuestionModel({
    question_id: 174,
    annotations: [topic_data[162]],
  }),
  new QuestionModel({
    question_id: 175,
    annotations: [topic_data[62]],
  }),
  new QuestionModel({
    question_id: 176,
    annotations: [
      topic_data[89],
      topic_data[18],
      topic_data[177],
      topic_data[141],
      topic_data[25],
    ],
  }),
  new QuestionModel({
    question_id: 177,
    annotations: [topic_data[68]],
  }),
  new QuestionModel({
    question_id: 178,
    annotations: [
      topic_data[54],
      topic_data[122],
      topic_data[157],
      topic_data[160],
      topic_data[176],
    ],
  }),
  new QuestionModel({
    question_id: 179,
    annotations: [topic_data[97], topic_data[80]],
  }),
  new QuestionModel({
    question_id: 180,
    annotations: [
      topic_data[45],
      topic_data[77],
      topic_data[128],
      topic_data[116],
    ],
  }),
  new QuestionModel({
    question_id: 181,
    annotations: [
      topic_data[135],
      topic_data[89],
      topic_data[95],
      topic_data[134],
    ],
  }),
  new QuestionModel({
    question_id: 182,
    annotations: [
      topic_data[93],
      topic_data[56],
      topic_data[63],
      topic_data[133],
      topic_data[153],
    ],
  }),
  new QuestionModel({
    question_id: 183,
    annotations: [topic_data[41], topic_data[160]],
  }),
  new QuestionModel({
    question_id: 184,
    annotations: [
      topic_data[160],
      topic_data[114],
      topic_data[163],
      topic_data[81],
    ],
  }),
  new QuestionModel({
    question_id: 185,
    annotations: [topic_data[167]],
  }),
  new QuestionModel({
    question_id: 186,
    annotations: [topic_data[173]],
  }),
  new QuestionModel({
    question_id: 187,
    annotations: [topic_data[24]],
  }),
  new QuestionModel({
    question_id: 188,
    annotations: [topic_data[100]],
  }),
  new QuestionModel({
    question_id: 189,
    annotations: [
      topic_data[66],
      topic_data[165],
      topic_data[151],
      topic_data[143],
    ],
  }),
  new QuestionModel({
    question_id: 190,
    annotations: [topic_data[121], topic_data[140], topic_data[101]],
  }),
  new QuestionModel({
    question_id: 191,
    annotations: [topic_data[120]],
  }),
  new QuestionModel({
    question_id: 192,
    annotations: [topic_data[74]],
  }),
  new QuestionModel({
    question_id: 193,
    annotations: [topic_data[98]],
  }),
  new QuestionModel({
    question_id: 194,
    annotations: [topic_data[59]],
  }),
  new QuestionModel({
    question_id: 195,
    annotations: [topic_data[54], topic_data[71], topic_data[116]],
  }),
  new QuestionModel({
    question_id: 196,
    annotations: [topic_data[151]],
  }),
  new QuestionModel({
    question_id: 197,
    annotations: [topic_data[119], topic_data[102], topic_data[30]],
  }),
  new QuestionModel({
    question_id: 198,
    annotations: [
      topic_data[119],
      topic_data[172],
      topic_data[62],
      topic_data[111],
      topic_data[54],
    ],
  }),
  new QuestionModel({
    question_id: 199,
    annotations: [topic_data[157]],
  }),
];

module.exports = question_data;
