const TopicModel = require("../server/api/topics/model");

const topic_data = [
  new TopicModel({
    name: "Cell Structure and Organisation",

  }),
  new TopicModel({
    name: "Movement of Substances",

  }),
  new TopicModel({
    name: "Biological Molecules",

  }),
  new TopicModel({
    name: "Nutrition in Humans",

  }),
  new TopicModel({
    name: "Nutrition in Plants",

  }),
  new TopicModel({
    name: "Transport in Flowering Plants",

  }),
  new TopicModel({
    name: "Transport in Humans",

  }),
  new TopicModel({
    name: "Respiration in Humans",

  }),
  new TopicModel({
    name: "Excretion in Humans",

  }),
  new TopicModel({
    name: "Homeostasis",

  }),
  new TopicModel({
    name: "Co-ordination and Response in Humans",

  }),
  new TopicModel({
    name: "Reproduction",

  }),
  new TopicModel({
    name: "Cell Division",

  }),
  new TopicModel({
    name: "Molecular Genetics",

  }),
  new TopicModel({
    name: "Inheritance",

  }),
  new TopicModel({
    name: "Organisms and their Environment",

  }),
  new TopicModel({
    name:
      "Identify cell structures (including organelles) of typical plant and animal cells from diagrams, photomicrographs and as seen under the light microscope using prepared slides and fresh material treated with an appropriate temporary staining technique:",

    parent: 0,
  }),
  new TopicModel({
    name:
      "Identify the following membrane systems and organelles from diagrams and electron micrographs:",

    parent: 0,
  }),
  new TopicModel({
    name:
      "State the functions of the membrane systems and organelles identified above",

    parent: 0,
  }),
  new TopicModel({
    name: "Compare the structure of typical animal and plant cells",

    parent: 0,
  }),
  new TopicModel({
    name:
      "State, in simple terms, the relationship between cell function and cell structure for the following:",

    parent: 0,
  }),
  new TopicModel({
    name: "Differentiate cell, tissue, organ and organ system",

    parent: 0,
  }),
  new TopicModel({
    name:
      "Define diffusion and describe its role in nutrient uptake and gaseous exchange in plants and humans",

    parent: 1,
  }),
  new TopicModel({
    name:
      "Define osmosis and describe the effects of osmosis on plant and animal tissues",

    parent: 1,
  }),
  new TopicModel({
    name:
      "Define active transport and discuss its importance as an energy-consuming process by which substances are transported against a concentration gradient, as in ion uptake by root hairs and uptake of glucose by cells in the villi",

    parent: 1,
  }),
  new TopicModel({
    name: "State the roles of water in living organisms",

    parent: 2,
  }),
  new TopicModel({
    name: "List the chemical elements which make up",

    parent: 2,
  }),
  new TopicModel({
    name: "Describe and carry out tests for",

    parent: 2,
  }),
  new TopicModel({
    name: "State that large molecules are synthesised from smaller basic units",

    parent: 2,
  }),
  new TopicModel({
    name: "Explain enzyme action in terms of the ‘lock and key’ hypothesis",

    parent: 2,
  }),
  new TopicModel({
    name:
      "Explain the mode of action of enzymes in terms of an active site, enzyme-substrate complex, lowering of activation energy and enzyme specificity",

    parent: 2,
  }),
  new TopicModel({
    name:
      "Investigate and explain the effects of temperature and pH on the rate of enzyme catalysed reactions",

    parent: 2,
  }),
  new TopicModel({
    name:
      "Describe the functions of main regions of the alimentary canal and the associated organs: mouth, salivary glands, oesophagus, stomach, duodenum, pancreas, gall bladder, liver, ileum, colon, rectum, anus, in relation to ingestion, digestion, absorption, assimilation and egestion of food, as appropriate",

    parent: 3,
  }),
  new TopicModel({
    name:
      "Describe peristalsis in terms of rhythmic wave-like contractions of the muscles to mix and propel the contents of the alimentary canal",

    parent: 3,
  }),
  new TopicModel({
    name:
      "Describe the functions of enzymes (e.g. amylase, maltase, protease, lipase) in digestion, listing the substrates and end-products",

    parent: 3,
  }),
  new TopicModel({
    name:
      "Describe the structure of a villus and its role, including the role of capillaries and lacteals in absorption",

    parent: 3,
  }),
  new TopicModel({
    name:
      "State the function of the hepatic portal vein as the transport of blood rich in absorbed nutrients from the small intestine to the liver",

    parent: 3,
  }),
  new TopicModel({
    name: "State the role of the liver in",

    parent: 3,
  }),
  new TopicModel({
    name:
      "Describe the effects of excessive consumption of alcohol: reduced self-control, depressant, effect on reaction times, damage to liver and social implications",

    parent: 3,
  }),
  new TopicModel({
    name:
      "Identify and label the cellular and tissue structure of a dicotyledonous leaf, as seen in transverse section using the light microscope and describe the significance of these features in terms of their functions, such as the",

    parent: 4,
  }),
  new TopicModel({
    name: "State the equation, in words and symbols, for photosynthesis",

    parent: 4,
  }),
  new TopicModel({
    name: "Describe the intake of carbon dioxide and water by plants",

    parent: 4,
  }),
  new TopicModel({
    name:
      "State that chlorophyll traps light energy and converts it into chemical energy for the formation of carbohydrates and their subsequent uses",

    parent: 4,
  }),
  new TopicModel({
    name:
      "Investigate and discuss the effects of varying light intensity, carbon dioxide concentration and temperature on the rate of photosynthesis (e.g. in submerged aquatic plant)",

    parent: 4,
  }),
  new TopicModel({
    name:
      "Discuss light intensity, carbon dioxide concentration and temperature as limiting factors on the rate of photosynthesis",

    parent: 4,
  }),
  new TopicModel({
    name:
      "Identify the positions and explain the functions of xylem vessels, phloem (sieve tube elements and companion cells) in sections of a herbaceous dicotyledonous leaf and stem, using the light microscope",

    parent: 5,
  }),
  new TopicModel({
    name:
      "Relate the structure and functions of root hairs to their surface area, and to water and ion uptake",

    parent: 5,
  }),
  new TopicModel({
    name:
      "Explain the movement of water between plant cells, and between them and the environment in terms of water potential (calculations on water potential are not required)",

    parent: 5,
  }),
  new TopicModel({
    name:
      "Outline the pathway by which water is transported from the roots to the leaves through the xylem vessels",

    parent: 5,
  }),
  new TopicModel({
    name:
      "Define the term transpiration and explain that transpiration is a consequence of gaseous exchange in plants",

    parent: 5,
  }),
  new TopicModel({
    name: "Describe and explain",

    parent: 5,
  }),
  new TopicModel({
    name:
      "Define the term translocation as the transport of food in the phloem tissue and illustrate the process through translocation studies",

    parent: 5,
  }),
  new TopicModel({
    name:
      "Identify the main blood vessels to and from the heart, lungs, liver and kidney",

    parent: 6,
  }),
  new TopicModel({
    name: "State the role of blood in transport and defence",

    parent: 6,
  }),
  new TopicModel({
    name:
      "List the different ABO blood groups and all possible combinations for the donor and recipient in blood transfusions",

    parent: 6,
  }),
  new TopicModel({
    name:
      "Relate the structure of arteries, veins and capillaries to their functions",

    parent: 6,
  }),
  new TopicModel({
    name:
      "Describe the transfer of materials between capillaries and tissue fluid",

    parent: 6,
  }),
  new TopicModel({
    name:
      "Describe the structure and function of the heart in terms of muscular contraction and the working of valves",

    parent: 6,
  }),
  new TopicModel({
    name:
      "Outline the cardiac cycle in terms of what happens during systole and diastole (histology of the heart muscle, names of nerves and transmitter substances are not required)",

    parent: 6,
  }),
  new TopicModel({
    name:
      "Describe coronary heart disease in terms of the occlusion of coronary arteries and list the possible causes, such as diet, stress and smoking, stating the possible preventative measures",

    parent: 6,
  }),
  new TopicModel({
    name:
      "Identify on diagrams and name the larynx, trachea, bronchi, bronchioles, alveoli and associated capillaries",

    parent: 7,
  }),
  new TopicModel({
    name:
      "State the characteristics of, and describe the role of, the exchange surface of the alveoli in gas exchange",

    parent: 7,
  }),
  new TopicModel({
    name:
      "Describe the removal of carbon dioxide from the lungs, including the role of the carbonic anhydrase enzyme",

    parent: 7,
  }),
  new TopicModel({
    name:
      "Describe the role of cilia, diaphragm, ribs and intercostal muscles in breathing",

    parent: 7,
  }),
  new TopicModel({
    name:
      "Describe the effect of tobacco smoke and its major toxic components – nicotine, tar and carbon monoxide, on health",

    parent: 7,
  }),
  new TopicModel({
    name:
      "Define and state the equation, in words and symbols, for aerobic respiration in humans",

    parent: 7,
  }),
  new TopicModel({
    name:
      "Define and state the equation, in words only, for anaerobic respiration in humans",

    parent: 7,
  }),
  new TopicModel({
    name: "Describe the effect of lactic acid in muscles during exercise",

    parent: 7,
  }),
  new TopicModel({
    name:
      "Define excretion and explain the importance of removing nitrogenous and other compounds from the body",

    parent: 8,
  }),
  new TopicModel({
    name:
      "Outline the function of the nephron with reference to ultra-filtration and selective reabsorption in the production of urine",

    parent: 8,
  }),
  new TopicModel({
    name: "Outline the role of anti-diuretic hormone (ADH) in osmoregulation",

    parent: 8,
  }),
  new TopicModel({
    name: "Outline the mechanism of dialysis in the case of kidney failure",

    parent: 8,
  }),
  new TopicModel({
    name:
      "Define homeostasis as the maintenance of a constant internal environment",

    parent: 9,
  }),
  new TopicModel({
    name:
      "Explain the basic principles of homeostasis in terms of stimulus resulting from a change in the internal environment, a corrective mechanism and negative feedback",

    parent: 9,
  }),
  new TopicModel({
    name:
      "Identify on a diagram of the skin: hairs, sweat glands, temperature receptors, blood vessels and fatty tissue",

    parent: 9,
  }),
  new TopicModel({
    name:
      "Describe the maintenance of a constant body temperature in humans in terms of insulation and the role of: temperature receptors in the skin, sweating, shivering, blood vessels near the skin surface and the co-ordinating role of the hypothalamus",

    parent: 9,
  }),
  new TopicModel({
    name:
      "State the relationship between receptors, the central nervous system and the effectors",

    parent: 10,
  }),
  new TopicModel({
    name:
      "Describe the structure of the eye as seen in front view and in horizontal section",

    parent: 10,
  }),
  new TopicModel({
    name:
      "State the principal functions of component parts of the eye in producing a focused image of near and distant objects on the retina",

    parent: 10,
  }),
  new TopicModel({
    name: "Describe the pupil reflex in response to bright and dim light",

    parent: 10,
  }),
  new TopicModel({
    name:
      "State that the nervous system – brain, spinal cord and nerves, serves to co-ordinate and regulate bodily functions",

    parent: 10,
  }),
  new TopicModel({
    name:
      "Outline the functions of sensory neurones, relay neurones and motor neurones",

    parent: 10,
  }),
  new TopicModel({
    name:
      "Discuss the function of the brain and spinal cord in producing a co-ordinated response as a result of a specific stimulus in a reflex action",

    parent: 10,
  }),
  new TopicModel({
    name:
      "Define a hormone as a chemical substance, produced by a gland, carried by the blood, which alters the activity of one or more specific target organs and is then destroyed by the liver",

    parent: 10,
  }),
  new TopicModel({
    name:
      "Explain what is meant by an endocrine gland, with reference to the islets of Langerhans in the pancreas",

    parent: 10,
  }),
  new TopicModel({
    name:
      "State the role of the hormone adrenaline in boosting blood glucose levels and give examples of situations in which this may occur",

  }),
  new TopicModel({
    name:
      "Explain how the blood glucose concentration is regulated by insulin and glucagon as a homeostatic mechanism",

    parent: 10,
  }),
  new TopicModel({
    name:
      "Describe the signs, such as an increased blood glucose level and glucose in urine, and the treatment of diabetes mellitus using insulin",

    parent: 10,
  }),
  new TopicModel({
    name:
      "Define asexual reproduction as the process resulting in the production of genetically identical offspring from one parent",

    parent: 11,
  }),
  new TopicModel({
    name:
      "Define sexual reproduction as the process involving the fusion of nuclei to form a zygote and the production of genetically dissimilar offspring",

    parent: 11,
  }),
  new TopicModel({
    name:
      "Identify and draw, using a hand lens if necessary, the sepals, petals, stamens and carpels of one, locally available, named, insect-pollinated, dicotyledonous flower, and examine the pollen grains using a microscope",

    parent: 11,
  }),
  new TopicModel({
    name: "State the functions of the sepals, petals, anthers and carpels",

    parent: 11,
  }),
  new TopicModel({
    name:
      "Use a hand lens to identify and describe the stamens and stigmas of one, locally available, named, wind-pollinated flower, and examine the pollen grains using a microscope",

    parent: 11,
  }),
  new TopicModel({
    name:
      "Outline the process of pollination and distinguish between self-pollination and cross-pollination",

    parent: 11,
  }),
  new TopicModel({
    name:
      "Compare, using fresh specimens, an insect-pollinated and a wind-pollinated flower",

    parent: 11,
  }),
  new TopicModel({
    name:
      "Describe the growth of the pollen tube and its entry into the ovule followed by fertilisation (production of endosperm and details of development are not required)",

    parent: 11,
  }),
  new TopicModel({
    name:
      "Identify on diagrams, the male reproductive system and give the functions of: testes, scrotum, sperm ducts, prostate gland, urethra and penis",

    parent: 11,
  }),
  new TopicModel({
    name:
      "Identify on diagrams, the female reproductive system and give the functions of: ovaries, oviducts, uterus, cervix and vagina",

    parent: 11,
  }),
  new TopicModel({
    name:
      "Briefly describe the menstrual cycle with reference to the alternation of menstruation and ovulation, the natural variation in its length, and the fertile and infertile phases of the cycle with reference to the effects of progesterone and estrogen only",

    parent: 11,
  }),
  new TopicModel({
    name:
      "Describe fertilisation and early development of the zygote simply in terms of the formation of a ball of cells which becomes implanted in the wall of the uterus",

    parent: 11,
  }),
  new TopicModel({
    name: "State the functions of the amniotic sac and the amniotic fluid",

    parent: 11,
  }),
  new TopicModel({
    name:
      "Describe the function of the placenta and umbilical cord in relation to exchange of dissolved nutrients, gases and excretory products (structural details are not required)",

    parent: 11,
  }),
  new TopicModel({
    name:
      "Discuss the spread of human immunodeficiency virus (HIV) and methods by which it may be controlled",

    parent: 11,
  }),
  new TopicModel({
    name:
      "State the importance of mitosis in growth, repair and asexual reproduction",

    parent: 12,
  }),
  new TopicModel({
    name: "Explain the need for the production of genetically identical cells",

    parent: 12,
  }),
  new TopicModel({
    name: "Identify, with the aid of diagrams, the main stages of mitosis",

    parent: 12,
  }),
  new TopicModel({
    name: "State what is meant by homologous pairs of chromosomes",

    parent: 12,
  }),
  new TopicModel({
    name:
      "Identify, with the aid of diagrams, the main stages of meiosis (names of the sub-divisions of prophase are not required)",

    parent: 12,
  }),
  new TopicModel({
    name:
      "Define the terms haploid and diploid, and explain the need for a reduction division process prior to fertilisation in sexual reproduction",

    parent: 12,
  }),
  new TopicModel({
    name: "State how meiosis and fertilisation can lead to variation",

    parent: 12,
  }),
  new TopicModel({
    name: "Outline the relationship between DNA, genes and chromosomes",

    parent: 13,
  }),
  new TopicModel({
    name:
      "State the structure of DNA in terms of the bases, sugar and phosphate groups found in each of their nucleotides",

    parent: 13,
  }),
  new TopicModel({
    name: "State the rule of complementary base pairing",

    parent: 13,
  }),
  new TopicModel({
    name:
      "State that DNA is used to carry the genetic code, which is used to synthesise specific polypeptides (details of transcription and translation are not required)",

    parent: 13,
  }),
  new TopicModel({
    name:
      "State that each gene is a sequence of nucleotides, as part of a DNA molecule",

    parent: 13,
  }),
  new TopicModel({
    name:
      "Explain that genes may be transferred between cells. Reference should be made to the transfer of genes between organisms of the same species or different species – transgenic plants or animals",

    parent: 13,
  }),
  new TopicModel({
    name:
      "Briefly explain how a gene that controls the production of human insulin can be inserted into bacterial DNA to produce human insulin in medical biotechnology",

    parent: 13,
  }),
  new TopicModel({
    name:
      "Discuss the social and ethical implications of genetic engineering, with reference to a named example",

    parent: 13,
  }),
  new TopicModel({
    name:
      "Define a gene as a unit of inheritance and distinguish clearly between the terms gene and allele",

    parent: 14,
  }),
  new TopicModel({
    name:
      "Explain the terms dominant, recessive, codominant, homozygous, heterozygous, phenotype and genotype",

    parent: 14,
  }),
  new TopicModel({
    name:
      "Predict the results of simple crosses with expected ratios of 3:1 and 1:1, using the terms homozygous, heterozygous, F1 generation and F2 generation",

    parent: 14,
  }),
  new TopicModel({
    name:
      "Explain why observed ratios often differ from expected ratios, especially when there are small numbers of progeny",

    parent: 14,
  }),
  new TopicModel({
    name:
      "Use genetic diagrams to solve problems involving monohybrid inheritance (genetic diagrams involving autosomal linkage or epistasis are not required)",

    parent: 14,
  }),
  new TopicModel({
    name:
      "Explain co-dominance and multiple alleles with reference to the inheritance of the ABO blood group phenotypes (A, B, AB and O) and the gene alleles (Iᴬ, Iᴮ and Iᴼ)",

    parent: 14,
  }),
  new TopicModel({
    name: "Describe the determination of sex in humans – XX and XY chromosomes",

    parent: 14,
  }),
  new TopicModel({
    name:
      "Describe mutation as a change in the structure of a gene such as in sickle cell anaemia, or in the chromosome number, such as the 47 chromosomes in the condition known as Down syndrome",

    parent: 14,
  }),
  new TopicModel({
    name:
      "Name radiation and chemicals as factors which may increase the rate of mutation",

    parent: 14,
  }),
  new TopicModel({
    name:
      "Describe the difference between continuous and discontinuous variation and give examples of each",

    parent: 14,
  }),
  new TopicModel({
    name:
      "State that variation and competition lead to differential survival of, and reproduction by, those organisms best fitted to the environment",

    parent: 14,
  }),
  new TopicModel({
    name:
      "Give examples of environmental factors that act as forces of natural selection",

    parent: 14,
  }),
  new TopicModel({
    name:
      "Explain the role of natural selection as a possible mechanism for evolution",

    parent: 14,
  }),
  new TopicModel({
    name:
      "Give examples of artificial selection such as in the production of economically important plants and animals",

    parent: 14,
  }),
  new TopicModel({
    name: "Briefly describe the non-cyclical nature of energy flow",

    parent: 15,
  }),
  new TopicModel({
    name:
      "Explain the terms producer, consumer and trophic level in the context of food chains and food webs",

    parent: 15,
  }),
  new TopicModel({
    name:
      "Explain how energy losses occur along food chains, and discuss the efficiency of energy transfer between trophic levels",

    parent: 15,
  }),
  new TopicModel({
    name: "Describe and interpret pyramids of numbers and biomass",

    parent: 15,
  }),
  new TopicModel({
    name:
      "Describe how carbon is cycled within an ecosystem and outline the role of forests and oceans as carbon sinks",

    parent: 15,
  }),
  new TopicModel({
    name: "Evaluate the effects of",

  }),
  new TopicModel({
    name:
      "Outline the roles of microorganisms in sewage treatment as an example of environmental biotechnology	Outline the roles of microorganisms in sewage treatment as an example of environmental biotechnology",

    parent: 15,
  }),
  new TopicModel({
    name:
      "Discuss reasons for conservation of species with reference to the maintenance of biodiversity and how this is done, e.g. management of fisheries and management of timber production",

    parent: 15,
  }),
  new TopicModel({
    name: "Chloroplasts",

    parent: 8,
  }),
  new TopicModel({
    name: "Cell surface membrane",

    parent: 8,
  }),
  new TopicModel({
    name: "Cell wall",

    parent: 8,
  }),
  new TopicModel({
    name: "Cytoplasm",

    parent: 8,
  }),
  new TopicModel({
    name:
      "Cell vacuoles (large, sap-filled in plant cells, small, temporary in animal cells)",

    parent: 8,
  }),
  new TopicModel({
    name: "Nucleus",

    parent: 8,
  }),
  new TopicModel({
    name: "Endoplasmic reticulum",

    parent: 9,
  }),
  new TopicModel({
    name: "Mitochondria",

    parent: 9,
  }),
  new TopicModel({
    name: "Golgi body",

    parent: 9,
  }),
  new TopicModel({
    name: "Ribosomes",

    parent: 9,
  }),
  new TopicModel({
    name: "Absorption – root hair cells",

    parent: 12,
  }),
  new TopicModel({
    name: "Conduction and support – xylem vessels",

    parent: 12,
  }),
  new TopicModel({
    name: "Transport of oxygen – red blood cells",

    parent: 12,
  }),
  new TopicModel({
    name: "Carbohydrates",

    parent: 18,
  }),
  new TopicModel({
    name: "Fats",

    parent: 18,
  }),
  new TopicModel({
    name: "Proteins",

    parent: 18,
  }),
  new TopicModel({
    name: "Starch (iodine in potassium iodide solution)",

    parent: 20,
  }),
  new TopicModel({
    name: "Reducing sugars (Benedict's solution)",

    parent: 20,
  }),
  new TopicModel({
    name: "Protein (biuret test)",

    parent: 20,
  }),
  new TopicModel({
    name: "Fats (ethanol emulsion)",

    parent: 20,
  }),
  new TopicModel({
    name: "Glycogen from glucose",

    parent: 21,
  }),
  new TopicModel({
    name: "Polypeptides and proteins from amino acids",

    parent: 21,
  }),
  new TopicModel({
    name: "Lipids such as fats from glycerol and fatty acids",

    parent: 21,
  }),
  new TopicModel({
    name: "Carbohydrate metabolism",

    parent: 30,
  }),
  new TopicModel({
    name: "Fat digestion",

    parent: 30,
  }),
  new TopicModel({
    name: "Breakdown of red blood cells",

    parent: 30,
  }),
  new TopicModel({
    name: "Metabolism of amino acids and the formation of urea",

    parent: 30,
  }),
  new TopicModel({
    name: "Breakdown of alcohol",

    parent: 30,
  }),
  new TopicModel({
    name: "Distribution of chloroplasts in photosynthesis",

    parent: 32,
  }),
  new TopicModel({
    name: "Stomata and mesophyll cells in gaseous exchange",

    parent: 32,
  }),
  new TopicModel({
    name: "Vascular bundles in transport",

    parent: 32,
  }),
  new TopicModel({
    name:
      "The effects of variation of air movement, temperature, humidity and light intensity on transpiration rate",

    parent: 43,
  }),
  new TopicModel({
    name: "How wilting occurs",

    parent: 43,
  }),
  new TopicModel({
    name: "Red blood cells – haemoglobin and oxygen transport",

    parent: 46,
  }),
  new TopicModel({
    name:
      "Plasma – transport of blood cells, ions, soluble food substances, hormones, carbon dioxide, urea, vitamins, plasma proteins",

    parent: 46,
  }),
  new TopicModel({
    name:
      "White blood cells – phagocytosis, antibody formation and tissue rejection",

    parent: 46,
  }),
  new TopicModel({
    name: "Platelets – fibrinogen to fibrin, causing clotting",

    parent: 46,
  }),
  new TopicModel({
    name: "Water pollution by sewage and by inorganic waste",

    parent: 130,
  }),
  new TopicModel({
    name:
      "Pollution due to insecticides including bioaccumulation up food chains and impact on top carnivores",

    parent: 130,
  }),
];

module.exports = topic_data;
