const router = require('express').Router();

router.use('/', require('./questions/router'));

module.exports = router;
