const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const config = require("../../config");

mongoose.connect(config.db.url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});

const QuestionModel = new Schema(
  {
    question_id: {
      type: Number,
      required: true,
    },
    annotations: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "topics",
        required: true,
      },
    ],
  },
  { validateBeforeSave: false }
);

module.exports = mongoose.model("questions", QuestionModel);
