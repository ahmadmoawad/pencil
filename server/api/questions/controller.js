const QuestionModel = require("./model");
const TopicModel = require("../topics/model");
const logger = require("../../util/logger");

exports.find = async (req, res, next) => {
  const search = req.query["q"];
  await QuestionModel.find()
    .populate({
      path: "annotations",
      match: { name: { $regex: search } },
    })
    .exec((err, result) => {
      vals = result.filter((val) => {
        return val.annotations.length > 0;
      });
      ids = vals.map((el) => {
        return el.question_id;
      });
      res.json(ids);
    });
};
