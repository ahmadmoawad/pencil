const router = require('express').Router();
const controller = require('./controller');

router.route('/search').get(controller.find);

module.exports = router;
