const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const config = require("../../config");
const autoIncrement = require("mongoose-auto-increment");

mongoose.connect(config.db.url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});

autoIncrement.initialize(mongoose.connection);
mongoose.set("useFindAndModify", false);

const TopicModel = new Schema({
  _id: {
    type: Number,
  },
  name: {
    type: String,
    unique: true,
    required: true,
  },
  parent: {
    type: Number,
    required: true,
    default: 0,
  },
  // future usage in favor of bidirectional
  questions: [
    { type: mongoose.Schema.Types.ObjectId, ref: "questions", required: false },
  ],
});

TopicModel.plugin(autoIncrement.plugin, {
  model: "topics",
  field: "_id",
  startWith: 0,
  incrementBy: 1,
});

module.exports = mongoose.model("topics", TopicModel);
