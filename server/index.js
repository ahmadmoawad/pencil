const express = require('express');
const api = require('./api');

const app = express();
require('./middleware')(app);

app.use('/', api);

module.exports = app;
