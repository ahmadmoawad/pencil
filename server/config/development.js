module.exports = {
    logging: false,
    seed: true,
    db: {
        url: 'mongodb://localhost:27017/learning-dev'
    }
};
