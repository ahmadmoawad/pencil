module.exports = {
    logging: false,
    seed: false,
    db: {
        url: 'mongodb://localhost:27017/learning-prod'
    }
};
